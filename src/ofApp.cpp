#include <iostream>
using std::cout, std::endl;

#include "ofApp.h"
#include "ofTrueTypeFont.h"


ofTrueTypeFont myfont;

int tictac = 0;

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(233,233,233);  // Sets the background color to red
  myfont.load("arial.ttf", 32);
}

//--------------------------------------------------------------
void ofApp::update(){
  ++tictac;
  cout << tictac << endl;

}

//--------------------------------------------------------------
void ofApp::draw(){
  ofSetColor(0, 0, 0);
  char tictacStr[255];
  sprintf(tictacStr, "time index: %i", tictac);
  myfont.drawString(tictacStr, 100,100);

  ofSetColor(0,250,255);
  ofFill();
  //ofDrawCircle(150,150,100);

  ofSetColor(0,0,255);
  ofFill();
  //ofDrawRectangle(10,10,100,100);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
