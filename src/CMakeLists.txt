# Create a library which includes the source file.
# The extension is already found. Any number of sources could be listed here.
add_library (Graph graph/Graph.cpp)

# Make sure the compiler can find include files for our library
# when other libraries or executables link to it.
target_include_directories (Graph PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
