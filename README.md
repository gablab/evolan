Created with `projectGenerator` (see https://openframeworks.cc/setup/linux-install/).

# Compile and run
`./build_and_run.sh`

# Test
`./run_tests.sh`
